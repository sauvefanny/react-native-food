import React, { useState } from "react";
import { View, Text, StyleSheet, ScrollView } from 'react-native'
import RestaurantList from "../components/RestaurantsList";
import SearchBar from "../components/SearchBar";
import useRestaurants from "../hooks/useRestaurants";

const SearchScreen = () => {

    const [term, setTerm] = useState('');
    const [searchApi, restaurants, errorMessage] = useRestaurants();

    const filterRestaurantByPrice = (price) => {
        return restaurants.filter(result => {
            return result.price === price
        })
    };

    return (
        <View style ={{flex :1}}>
            <SearchBar
                term={term}
                onTermChange={setTerm}
                onTermSubmit={() => searchApi(term)} />
            {errorMessage ?
                <Text>{errorMessage}</Text>
                : null}
            <ScrollView >
                < RestaurantList results={filterRestaurantByPrice('$')} title="Cost Effective" />
                < RestaurantList results={filterRestaurantByPrice('$$')} title="Big Pricer" />
                < RestaurantList results={filterRestaurantByPrice('$$$')} title="Big Spencer" />
            </ScrollView>
        </View>
    )
};

style = StyleSheet.create({});

export default SearchScreen;