import React, { useEffect, useState } from "react";
import { View, StyleSheet, Text, Image } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import yelp from "../api/yelp";

const RestaurantsShowScreen = ( {navigation}) => {
    
    const [restaurant , setRestaurant] = useState(null) //if null important
    const id = navigation.getParam('id');

    console.log(restaurant)

    const getRestaurant = async id =>{
        const response = await yelp.get(`/${id}`);
        setRestaurant(response.data)
    };

    useEffect(() =>{
        getRestaurant(id)
    }, []);

    if(!restaurant){
        return null
    }

    return(
        <View>
            <Text>{restaurant.name}</Text>
            <FlatList
                data={restaurant.photos}
                keyExtractor={(photo) => photo}
                renderItem={({item}) => {
                    return <Image style={styles.image} source={{uri : item }}/>
                }}
            />
        </View>
    )
};

const styles = StyleSheet.create({
    image :{
        width : 300,
        height : 200, 
    }
});

export default RestaurantsShowScreen;