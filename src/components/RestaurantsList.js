import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import RestaurantsDetail from "./RestaurantsDetail";
import { withNavigation } from "react-navigation";


const RestaurantList = ({ title, results, navigation }) => {

    if (!results.length) {
        return null;
    };

    return (
        <View>
            <Text style={styles.title}>{title}</Text>
            <FlatList
                horizontal
                data={results}
                keyExtractor={result => result.id}
                renderItem={({ item }) => {
                    return (
                        <TouchableOpacity
                            onPress={() => navigation.navigate('RestaurantsShow', {id : item.id})}
                        >
                            <RestaurantsDetail result={item} />
                        </TouchableOpacity>
                    );
                }}
            />
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        marginBottom: 10,
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
        marginBottom: 15
    },
});

export default withNavigation(RestaurantList);